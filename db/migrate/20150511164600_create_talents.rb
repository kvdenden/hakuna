class CreateTalents < ActiveRecord::Migration
  def change
    create_table :talents do |t|
      t.string :title
      t.integer :position
      t.string :ancestry, index: true

      t.timestamps null: false
    end

    create_join_table :users, :talents
  end
end
