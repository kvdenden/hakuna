class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.text :job_type
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.date :birthday
      t.string :gender
      t.string :nationality
      t.text :ethnicity
      t.integer :height
      t.integer :weight
      t.integer :shoe_size
      # t.string :clothing_size
      # t.boolean :children_clothing_size
      t.string :eye_color
      t.string :hair_color
      t.string :hair_length
      t.string :hair_type
      t.boolean :haircut
      t.boolean :beard
      t.boolean :mustache
      t.boolean :tattoos
      t.boolean :piercings
      t.string :employment
      t.boolean :driving_license
      t.boolean :own_transport
      t.text :available_for
      t.text :previous_experience

      t.timestamps null: false
    end
  end
end
