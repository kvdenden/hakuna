class AddClothingSizeToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :clothing_size, :string
  end
end
