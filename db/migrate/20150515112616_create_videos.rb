class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.references :videoable, polymorphic: true, index: true
      t.string :provider
      t.string :url
      t.integer :position

      t.timestamps null: false
    end
  end
end
