class AddImportToUsers < ActiveRecord::Migration
  def change
    add_column :users, :import, :boolean, default: false
  end
end
