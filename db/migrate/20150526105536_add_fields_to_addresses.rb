class AddFieldsToAddresses < ActiveRecord::Migration
  def up
    add_column :addresses, :street, :string
    add_column :addresses, :number, :string
    add_column :addresses, :bus, :string
    Address.all.each do |address|
      address.street = address.line1
      address.save
    end
    remove_column :addresses, :line1
    remove_column :addresses, :line2
  end

  def down
    add_column :addresses, :line1, :string
    add_column :addresses, :line2, :string
    Address.all.each do |address|
      if address.bus.blank?
        address.line1 = "#{address.street} #{address.number}"
      else
        address.line1 = "#{address.street} #{address.number}/#{address.bus}"
      end
      address.save
    end
    remove_column :addresses, :street
    remove_column :addresses, :number
    remove_column :addresses, :bus
  end
end
