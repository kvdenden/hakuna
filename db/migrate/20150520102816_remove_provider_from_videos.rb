class RemoveProviderFromVideos < ActiveRecord::Migration
  def change
    remove_column :videos, :provider
  end
end
