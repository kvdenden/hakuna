# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

talents = {
  "Cast" => {
    "Acting" => {
      "Drama" => {},
      "Fiction" => {},
      "Theatre" => {},
      "Other" => {},
    },
    "Dance" => {
      "Ballet" => {},
      "Breakdance" => {},
      "Hip hop" => {},
      "Lyrical hop hop" => {},
      "Modern" => {},
      "Popping & locking" => {},
      "Housedance" => {},
      "Waacking" => {},
      "African dance" => {},
      "Ballroom dancing" => {},
      "Belly dance" => {},
      "Crumping" => {},
      "Tap dance" => {},
      "Dancehall" => {},
      "Freestyle" => {},
      "Other" => {}
    },
    "Singing" => {
      "Opera" => {},
      "Rap" => {},
      "Soul & Jazz" => {},
      "Rock" => {},
      "Pop" => {},
      "Hip hop" => {},
      "Classical" => {},
      "R&B" => {},
      "Other" => {}
    },
    "Music" => {
      "Piano" => {},
      "Guitar" => {},
      "Saxophone" => {},
      "Violin" => {},
      "Drums" => {},
      "Bass" => {},
      "DJ / Producer" => {},
      "Other" => {}
    },
    "Sports" => {
      "Soccer" => {},
      "Basketball" => {},
      "Volleyball" => {},
      "Athletics" => {},
      "Swimming" => {},
      "Tennis" => {},
      "Combat sports" => {},
      "Drama" => {}
    },
    "Modeling" => {}

  },
  "Crew" => {
    "Camera" => {
      "Director of photography" => {},
      "Camera operator" => {},
      "First assistant camera operator" => {},
      "Focus puller" => {},
      "Grip" => {},
      "Steadicam operator" => {},
      "Clapper loader" => {}
    },
    "Production" => {
      "Producer" => {},
      "Executive producer" => {},
      "Production manager" => {},
      "Assistant production manager" => {},
      "Director" => {},
      "Assistant director" => {},
      "Runner" => {},
      "Location manager" => {}
    },
    "Lighting" => {
      "Gaffer" => {},
      "Electrician" => {},
      "Light assistant" => {},
      "Best boy" => {}
    },
    "Art department" => {
      "Production designer" => {},
      "Art director" => {},
      "Assistant art director" => {},
      "Set designer" => {},
      "Set decorator" => {},
      "Prop master" => {},
      "Make-up artist" => {},
      "Hairdresser" => {},
      "Costume designer" => {},
      "Stylist" => {},
      "Graphic design" => {},
      "Photographer" => {}
    },
    "Sound" => {
      "Sound director" => {},
      "Boom operator" => {},
      "Music supervisor" => {},
      "Re-recording mixer" => {},
      "Componist" => {},
      "Sound designer" => {},
      "Sound editor" => {}
    }
  }
}

def create_talent title, children = {}, parent = nil
  talent = Talent.new title: title
  talent.parent = parent
  talent.save
  children.each do |child, grandchildren|
    create_talent child, grandchildren, talent
  end
  return talent
end

talents.each do |title, children|
  create_talent title, children, nil
end
