# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

update_user_form = () ->
  calc_age = (birthday) ->
    birthday = new Date(birthday)
    today = new Date()
    age = today.getFullYear() - birthday.getFullYear()
    age -= 1 if today.getMonth() < birthday.getMonth() || (today.getMonth() == birthday.getMonth() && today.getDate() < birthday.getDate())
    age



  window.calc_age = calc_age
  hide_associations_marked_for_destruction()
  $("form.user").find('.show-cast').toggle($("#user_profile_attributes_job_type_cast").is(":checked"))
  $("form.user").find('.show-crew').toggle($("#user_profile_attributes_job_type_crew").is(":checked"))

  $("form.user").find('.show-male').toggle($("#user_profile_attributes_gender_male").is(":checked"))
  $("form.user").find('.show-female').toggle($("#user_profile_attributes_gender_female").is(":checked"))

  birthday = moment($("#user_profile_attributes_birthday").val(), "DD/MM/YYYY").toDate()
  if isNaN(birthday.getTime())
    # invalid date
    $("form.user").find('.show-adult, .show-child').hide()
  else
    age = calc_age(birthday)
    $("form.user").find('.show-adult.show-male').toggle($("#user_profile_attributes_gender_male").is(":checked") && age > 16)
    $("form.user").find('.show-adult.show-female').toggle($("#user_profile_attributes_gender_female").is(":checked") && age > 16)
    $("form.user").find('.show-child').toggle(age <= 16)

  $("form.user").find('.show-cast, .show-crew, .show-male, .show-female, .show-adult, .show-child').filter('.clear-hidden:hidden').find('select').val('')
  $("form.user").find('.show-cast, .show-crew, .show-male, .show-female, .show-adult, .show-child').filter('.clear-hidden:hidden').find('input[type="checkbox"]').prop('checked', '')

init_datepicker = () ->
  $(".date-picker").datepicker
    format: 'dd/mm/yyyy'

init_sortable = () ->
  $(".sortable-fields").sortable({items: "> li"})

hide_associations_marked_for_destruction = () ->
  $(".nested-fields input[type='hidden'][value='true']").parent().hide()

init_select2 = () ->
  $("select[multiple='multiple']").select2();

init_media = () ->
  $(".image-container a.colorbox").colorbox({maxWidth: '90%', maxHeight: '80%', scrolling: false});

ready = () ->
  $("#user_profile_attributes_job_type_input input[type='checkbox'], #user_profile_attributes_gender_input input[type='radio'], #user_profile_attributes_birthday_input input").change update_user_form
  update_user_form()
  init_sortable()
  init_select2()
  init_media()
  init_datepicker()

$(document).ready(ready)
$(document).on('page:load', ready)
