ActiveAdmin.register User do

  # xlsx do
  #   column('name') do |user|
  #     user.full_name
  #   end
  # end

  config.per_page = 48

  actions :all, except: [:new]

  config.batch_actions = false

  scope :all
  scope :active
  scope :inactive
  scope :admin
  scope :cast
  scope :crew

  filter :full_name, as: :string, label: I18n.t(:name)

  filter :email_cont, label: I18n.t(:email)

  filter :city, as: :string

  filter :talent, as: :select, collection: Talent.all

  filter :lists#, as: :select, collection: List.all

  filter :profile_age, as: :numeric_range, wrapper_html: {class: 'filter_form_field'}, label: I18n.t(:age)
  filter :profile_weight, as: :numeric_range, wrapper_html: {class: 'filter_form_field'}, label: I18n.t(:weight)
  filter :profile_height, as: :numeric_range, wrapper_html: {class: 'filter_form_field'}, label: I18n.t(:height)

  filter :profile_clothing_size, as: :select, collection: ClothingSize.to_select, label: I18n.t(:clothing_size)

  filter :profile_gender, as: :select, collection: Profile.gender.options, label: I18n.t(:gender)
  filter :profile_ethnicity_cont, as: :select, collection: Profile.ethnicity.options, label: I18n.t(:ethnicity)
  filter :profile_nationality, as: :select, collection: ISO3166::Country.all.sort_by {|c| c[0].parameterize }, label: I18n.t(:nationality)
  filter :profile_language_cont, as: :select, collection: LanguageList::COMMON_LANGUAGES.map{|l|[l.name, l.iso_639_1]}, label: I18n.t(:language)

  filter :profile_eye_color, as: :select, collection: Profile.eye_color.options, label: I18n.t(:eye_color)
  filter :profile_hair_color, as: :select, collection: Profile.hair_color.options, label: I18n.t(:hair_color)
  filter :profile_hair_length, as: :select, collection: Profile.hair_length.options, label: I18n.t(:hair_length)
  filter :profile_hair_type, as: :select, collection: Profile.hair_type.options, label: I18n.t(:hair_type)
  filter :profile_haircut, as: :boolean, label: I18n.t(:haircut)
  filter :profile_beard, as: :boolean, label: I18n.t(:beard)
  filter :profile_mustache, as: :boolean, label: I18n.t(:mustache)
  filter :profile_tattoos, as: :boolean, label: I18n.t(:tattoos)
  filter :profile_piercings, as: :boolean, label: I18n.t(:piercings)

  filter :profile_employment, as: :select, collection: Profile.employment.options, label: I18n.t(:employment)
  filter :profile_driving_license, as: :boolean, label: I18n.t(:driving_license)
  filter :profile_own_transport, as: :boolean, label: I18n.t(:own_transport)

  filter :created_at, as: :date_range, wrapper_html: {class: 'filter_form_field'}, label: I18n.t(:registered_at)
  filter :updated_at, as: :date_range, wrapper_html: {class: 'filter_form_field'}, label: I18n.t(:updated_at)


  permit_params :email, :password, :password_confirmation, :current_password, :admin, :active, profile_attributes: [
    :id,
    { :job_type => [] },
    :first_name,
    :last_name,
    :phone_number,
    :birthday,
    :gender,
    :nationality,
    { :ethnicity => [] },
    :height,
    :weight,
    :shoe_size,
    # :clothing_size,
    # :children_clothing_size,
    :eye_color,
    :hair_color,
    :hair_length,
    :hair_type,
    :haircut,
    :beard,
    :mustache,
    :tattoos,
    :piercings,
    :employment,
    :driving_license,
    :own_transport,
    { :available_for => [] },
    :previous_experience,
    address_attributes: [
      :id,
      :line1,
      :line2,
      :city,
      :country,
      :postal_code
    ]
    ]

  controller do

    def update
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end

    def scoped_collection
      super.includes :profile
    end

  end

  action_item :import, only: :index do
    link_to I18n.t(:import_users), import_admin_users_path
  end

  collection_action :import, method: :get do
  end

  collection_action :import_users, method: :post do
    if params[:import].present? && params[:import][:file].present?
      csv = CSV.read(params[:import][:file].path, headers: true)
      csv.each do |row|
        Delayed::Job.enqueue ImportJob.new(row)
      end
      redirect_to collection_path, notice: t(:import_users_complete)
    else
      redirect_to import_admin_users_path, notice: t(:file_missing)
    end
  end

  # index do
  #   selectable_column
  #   # id_column
  #   column :email
  #   column :full_name, label: :name
  #
  #   actions dropdown: true
  # end

  index as: :block do |user|
    div for: user do
      div class: 'wrapper' do
        div link_to(image_tag(user.profile.images.present? ? user.profile.images.first.file.thumb.url : Image.new.file.thumb.url ), profile_path(user.id)), class: 'user-image'
        h3 link_to(user.full_name, profile_path(user.id)), class: 'user-full-name'
      end
      div class: 'actions' do
        span link_to('edit', edit_profile_path(user.id))
        span link_to('delete', admin_user_path(user.id), data: { confirm: "Are you sure?" }, method: :delete)
      end
    end
  end


  show do |user|
    attributes_table do
      row :email
      row :administrator do
        status_tag user.admin? ? "yes" : "no"
      end
      row :status do
        if user.active?
          status_tag('active', :ok)
        else
          status_tag('blocked', :error)
        end
      end
    end
  end

  form html: {multipart: true} do |f|
    f.inputs "Account details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :admin, label: :administrator
      f.input :active
    end
    f.actions
  end

  csv force_quotes: true, col_sep: ';' do
    column(:name) { |user| user.full_name }
    column(:email)
    column(:phone_number) { |user| user.profile.phone_number }
    column(:address) { |user| user.profile.address.to_s }
    column(:birthday) { |user| user.profile.birthday }
    column(:gender) { |user| user.profile.gender }
  end

  # config.xlsx_builder.delete_columns :id, :encrypted_password, :created_at, :updated_at

  # xlsx(:i18n_scope => [:active_admin, :xlsx, :user],
  #      :header_style => {:bg_color => 'FF0000', :fg_color => 'FF' }) do
  #   column(:name) { |user| user.full_name }
  #   column(:email)
  #   column(:phone_number) { |user| user.profile.phone_number }
  #   column(:address) { |user| user.profile.address.to_s }
  #   column(:birthday) { |user| user.profile.birthday }
  #   column(:gender) { |user| user.profile.gender }
  # end

end

# permit_params :email, :password, :password_confirmation, :current_password, :admin, :active, profile_attributes: [
#   :id,
#   { :job_type => [] },
#   :first_name,
#   :last_name,
#   :phone_number,
#   :birthday,
#   :gender,
#   :nationality,
#   { :ethnicity => [] },
#   :height,
#   :weight,
#   :shoe_size,
#   # :clothing_size,
#   # :children_clothing_size,
#   :eye_color,
#   :hair_color,
#   :hair_length,
#   :hair_type,
#   :haircut,
#   :beard,
#   :mustache,
#   :tattoos,
#   :piercings,
#   :employment,
#   :driving_license,
#   :own_transport,
#   { :available_for => [] },
#   :previous_experience,
#   address_attributes: [
#     :id,
#     :line1,
#     :line2,
#     :city,
#     :country,
#     :postal_code
#   ]
#   ]
