ActiveAdmin.register List do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  actions :all, except: [:show]
  permit_params :title
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  index do
    column :title
    column :users do |list|
      list.users.count
    end
    actions
  end

  show do |list|
    ul do
      list.users.each do |u|
        li do
          link_to u.full_name, profile_path(u)
        end
      end
    end
  end


end
