ActiveAdmin.register Talent do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

actions :all, except: [:show]

  permit_params :title

  config.filters = false

  config.sort_order = 'position_asc'
  config.paginate = false

  sortable tree: true

  index as: :sortable do
    label do |item|
      item.to_s
    end
    # column :title
    #
    # label do |item|
    #   if item.link.blank?
    #     item.title
    #   else
    #     link_to item.title, URI(item.link.to_s).host ? URI(item.link).to_s : URI.join(root_url, item.link).to_s
    #   end
    # end
    actions only: [:show]
  end

  form do |f|
    f.inputs do
      f.input :title
      # f.input :link
    end
    f.actions
  end

  show do |item|
    attributes_table do
      row :title
      # row :link do
      #   unless item.link.blank?
      #     link_to item.link, URI(item.link.to_s).host ? URI(item.link).to_s : URI.join(root_url, item.link).to_s
      #   end
      # end
      # row :created_at
      # row :updated_at
    end
  end

  controller do
    def find_resource
      begin
        scoped_collection.where(slug: params[:id]).first!
      rescue ActiveRecord::RecordNotFound
        scoped_collection.find(params[:id])
      end
    end
  end


end
