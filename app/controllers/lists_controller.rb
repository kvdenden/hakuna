class ListsController < ApplicationController

  def add_user
    @list = List.find_by_id(params[:list_id])
    @user = User.find_by_id(params[:user_id])
    if @list
      if @user && can?(:update, @list)
        @list.users << @user
      end
    end
    redirect_to profile_path(@user)
  end

  def remove_user
    @list = List.find_by_id(params[:list_id])
    @user = User.find_by_id(params[:user_id])
    if @list
      if @user && can?(:update, @list)
        @list.users.delete(@user)
      end
    end
    redirect_to profile_path(@user)
  end
end
