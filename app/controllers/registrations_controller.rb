class RegistrationsController < Devise::RegistrationsController
  def create
    # set_positions params[:user][:profile_attributes][:images_attributes]
    # set_positions params[:user][:profile_attributes][:videos_attributes]
    super
    begin
      UserMailer.registered(resource).deliver unless resource.invalid?
    rescue
    end
  end

  def update
    set_positions params[:user][:profile_attributes][:images_attributes]
    set_positions params[:user][:profile_attributes][:videos_attributes]
    # if params[:user][:password].blank?
    #   params[:user].delete(:password)
    #   params[:user].delete(:password_confirmation)
    # end
    super
  end

  protected

  def set_positions items
    items.each_with_index do |item, index|
      key, value = item
      value['position'] = index
    end if items
  end

  # def update_resource resource, params
  #   resource.update_without_password params
  # end
end
