class CommentsController < ApplicationController

  def comment
    @user = User.find_by_id(params[:user_id])
    if @user && can?(:update, @user)
      @comment = Comment.where(user_id: @user.id).first_or_initialize
      @comment.update_attributes text: params[:comment]
      redirect_to profile_path(@user), notice: 'Comment has been updated successfully.'
    else
      flash[:error] = I18n.t(:permission_denied)
      redirect_to home_path
    end
  end

end
