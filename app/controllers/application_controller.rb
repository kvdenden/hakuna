class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_filter :configure_permitted_parameters, if: :devise_controller?

  def authenticate_active_admin_user!
    authenticate_user!
    unless current_user.admin?
      flash[:alert] = "Unauthorized Access!"
      redirect_to root_path
    end
  end

  def model_errors resource
    messages = resource.errors.messages.map { |key, msgs| msgs.map{ |msg| "<li>#{key.to_s.split('.').last.humanize} #{msg}</li>" } }.flatten.join
    sentence = I18n.t('errors.messages.not_saved',
    count: resource.errors.count,
    resource: resource.class.model_name.human.downcase)
    html = "<h4>#{sentence}</h4>"
    html << "<ul>#{messages}</ul>"
    html.html_safe
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:email, :password, :password_confirmation, {:talent_ids => []}, :terms_and_conditions, profile_attributes: [
        :id,
        { :job_type => [] },
        :first_name,
        :last_name,
        :phone_number,
        :birthday,
        :gender,
        :nationality,
        { :ethnicity => [] },
        { :language => [] },
        :height,
        :weight,
        :shoe_size,
        :clothing_size,
        # :children_clothing_size,
        :eye_color,
        :hair_color,
        :hair_length,
        :hair_type,
        :haircut,
        :beard,
        :mustache,
        :tattoos,
        :piercings,
        :employment,
        :driving_license,
        :own_transport,
        { :available_for => [] },
        :previous_experience,
        address_attributes: [
          :id,
          :street,
          :number,
          :bus,
          :city,
          :country,
          :postal_code
        ],
        images_attributes: [:id, :file, :file_cache, :position, :_destroy],
        videos_attributes: [:id, :title, :url, :position, :_destroy]
        ])
    end

    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:email, :password, :password_confirmation, :current_password, {:talent_ids => []}, profile_attributes: [
        :id,
        { :job_type => [] },
        :first_name,
        :last_name,
        :phone_number,
        :birthday,
        :gender,
        :nationality,
        { :ethnicity => [] },
        { :language => [] },
        :height,
        :weight,
        :shoe_size,
        :clothing_size,
        :eye_color,
        :hair_color,
        :hair_length,
        :hair_type,
        :haircut,
        :beard,
        :mustache,
        :tattoos,
        :piercings,
        :employment,
        :driving_license,
        :own_transport,
        { :available_for => [] },
        :previous_experience,
        address_attributes: [
          :id,
          :street,
          :number,
          :bus,
          :city,
          :country,
          :postal_code
        ],
        images_attributes: [:id, :file, :file_cache, :position, :_destroy],
        videos_attributes: [:id, :provider, :url, :position, :_destroy]
        ])
    end
  end
end
