class MainController < ApplicationController

  def home
    redirect_to my_profile_path if current_user
  end


  # GET /headhunt
  def headhunt
  end

  # POST /headhunt
  def headhunted
    begin
      email = params[:email]
      HeadhuntMailer.headhunted(email).deliver # mail to user
      flash[:notice] = I18n.t(:headhunted)
    rescue
    ensure
      redirect_to headhunt_path
    end
  end

end
