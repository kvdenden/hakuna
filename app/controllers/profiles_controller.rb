class ProfilesController < ApplicationController

  def my
    @user = current_user
    @lists = @user ? @user.lists : []
    if @user && can?(:read, @user)
      @comment = Comment.where(user_id: @user.id).first_or_initialize
      if !@user.profile.valid?
        flash.now[:alert] = invalid_profile_warning @user
      end
      render "show"
    else
      flash[:error] = I18n.t(:permission_denied)
      redirect_to home_path
    end
  end

  def show
    @user = User.find_by_id(params[:id])
    @lists = @user ? @user.lists : []
    if @user && can?(:read, @user)
      @comment = Comment.where(user_id: @user.id).first_or_initialize
    else
      flash[:error] = I18n.t(:permission_denied)
      redirect_to home_path
    end
  end

  def edit
    @user = User.find_by_id(params[:id])
    if @user && can?(:update, @user)
      render locals: { resource: @user }
    else
      flash[:error] = I18n.t(:permission_denied)
      redirect_to home_path
    end
  end

  def update
    @user = User.find_by_id(params[:id])
    if @user && can?(:update, @user)
      set_positions params[:user][:profile_attributes][:images_attributes]
      set_positions params[:user][:profile_attributes][:videos_attributes]
      if @user.update_attributes user_parameters
        redirect_to profile_path(@user), notice: 'User has been updated successfully.'
      else
        flash.now[:error] = model_errors @user
        render 'edit', locals: { resource: @user }
      end
    else
      flash[:error] = I18n.t(:permission_denied)
      redirect_to home_path
    end
  end

  protected

  def set_positions items
    items.each_with_index do |item, index|
      key, value = item
      value['position'] = index + 1
    end if items
  end

  private
  def user_parameters
    params.require(:user).permit(:email, :active, :admin, {:talent_ids => []}, profile_attributes: [
      :id,
      { :job_type => [] },
      :first_name,
      :last_name,
      :phone_number,
      :birthday,
      :gender,
      :nationality,
      { :ethnicity => [] },
      { :language => [] },
      :height,
      :weight,
      :shoe_size,
      :clothing_size,
      :eye_color,
      :hair_color,
      :hair_length,
      :hair_type,
      :haircut,
      :beard,
      :mustache,
      :tattoos,
      :piercings,
      :employment,
      :driving_license,
      :own_transport,
      { :available_for => [] },
      :previous_experience,
      address_attributes: [
        :id,
        :street,
        :number,
        :bus,
        :city,
        :country,
        :postal_code
      ],
      images_attributes: [:id, :file, :file_cache, :position, :_destroy],
      videos_attributes: [:id, :title, :url, :position, :_destroy]
      ])
  end

  def invalid_profile_warning user
    warning = "<h4>#{I18n.t(:invalid_profile)}</h4>"
    warning += "<ul>" + user.profile.errors.full_messages.map { |msg| "<li>#{msg}</li>" }.join('') + "</ul>"
    warning += "<p><br /><a href='#{edit_profile_path(user)}' class='alert-link'>#{I18n.t(:edit_profile)}</a></p>"
    return warning.html_safe
  end

end
