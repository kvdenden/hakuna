module ImportHelper
  def self.process_csv(input_file, output_file)
    CSV.open(output_file, 'wb') do |output_csv|
      header = %w( email job_type first_name last_name street number bus city postal_code phone_number birthday gender nationality ethnicity languages height weight shoe_size clothing_size eye_color hair_color hair_length hair_type haircut beard mustache tattoos piercings employment driving_license own_transport previous_experience available_for talent_ids images videos )

      output_csv << header

      attribute_maps = {
        ethnicity: {
          'caucasian': 'white',
          'easterneuropean': 'eastern_european',
          'latinamerican': 'latin_american',
          'middleeastern': 'middle_eastern',
          'nativeamerican': 'native_american',
          'northafrican': 'north_african'
        },
        eye_color: {
          'lightblue': 'light_blue',
          'brightblue': 'bright_blue',
          'darkbrown': 'dark_brown',
          'lightbrown': 'light_brown'
        },
        hair_color: {
          'verydarkbrown': 'very_dark_brown',
          'lightbrown': 'light_brown',
          'darkblonde': 'dark_blonde',
          'lightblonde': 'light_blonde',
          'platinumblonde': 'platinum_blonde'
        },
        hair_length: {
          'veryshort': 'very_short',
          'verylong': 'very_long'
        },
        employment: {
          'selfemployed': 'self_employed'
        },
        available_for: {
          'unrecognizablenude': 'unrecognizable_nude',
          'artisticnude': 'artistic_nude'
        },
        clothing_size: {
          'xs0_f': 'xs32_f',
          'xs_f': 'xs34_f',
          's_f': 's36_f',
          'm_f': 'm38_f',
          'l1_f': 'l42_f',
          'l2_f': 'l42_44_f',
          'l3_f': 'l44_f',
          'xl_f': 'xl46_f',
          'l_f': 'l38_40_f',
          'xs0_m': 'xs44_m',
          'xs_m': 'xs46_m',
          's_m': 's48_m',
          'm_m': 'm50_m',
          'xl_m': 'xl54_m',
          'l_m': 'l52_m'
        },
        talent_ids: {
          'Drama': '3',
          'Fiction': '4',
          'Theatre': '5',
          'Other (Acting)': '6',
          'Ballet': '8',
          'Breakdance': '9',
          'Hip hop (Dance)': '10',
          'Lyrical hip hop': '11',
          'Modern': '12',
          'Popping & locking': '13',
          'Housedance': '14',
          'Waacking': '15',
          'African dance': '16',
          'Ballroom dancing': '17',
          'Belly dance': '18',
          'Crumping': '19',
          'Tap dance': '20',
          'Dancehall': '21',
          'Freestyle': '22',
          'Other (Dance)': '23',
          'Opera': '25',
          'Rap': '26',
          'Soul & Jazz': '27',
          'Rock': '28',
          'Pop': '29',
          'Hip hop': '30',
          'Classical': '31',
          'R&B': '32',
          'Other (Singing)': '33',
          'Piano': '35',
          'Guitar': '36',
          'Saxophone': '37',
          'Violin': '38',
          'Drums': '39',
          'Bass': '40',
          'DJ / Producer': '41',
          'Other (Music)': '42',
          'Soccer': '44',
          'Basketball': '45',
          'Volleyball': '46',
          'Athletics': '47',
          'Swimming': '48',
          'Tennis': '49',
          'Combat sports': '50',
          'Other (Sports)': '51',
          'Modeling': '52',

          'Director of photography': '55',
          'Camera operator': '56',
          'First assistant camera operator': '57',
          'Focus puller': '58',
          'Grip': '59',
          'Steadicam operator': '60',
          'Clapper loader': '61',
          'Executive producer': '64',
          'Producer': '63',
          'Assistant production manager': '66',
          'Production manager': '65',
          'Assistant director': '68',
          'Director': '67',
          'Runner': '69',
          'Location manager': '70',
          'Gaffer': '72',
          'Electrician': '73',
          'Light assistant': '74',
          'Best boy': '75',
          'Production designer': '77',
          'Assistant art director': '79',
          'Art director': '78',
          'Set designer': '80',
          'Set decorator': '81',
          'Prop master': '82',
          'Make-up artist': '83',
          'Hairdresser': '84',
          'Costume designer': '85',
          'Stylist': '86',
          'Graphic design': '87',
          'Photographer': '88',
          'Sound director': '90',
          'Boom operator': '91',
          'Music supervisor': '92',
          'Re-recording mixer': '93',
          'Componist': '94',
          'Sound designer': '95',
          'Sound editor': '96',

          'Acting': '3, 4, 5, 6',
          'Dance': '8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23',
          'Singing': '25, 26, 27, 28, 29, 30, 31, 32, 33',
          'Music': '35, 36, 37, 38, 39, 40, 41, 42',
          'Sports': '44, 45, 46, 47, 48, 49, 50, 51',
          'Camera': '55, 56, 57, 58, 59, 60, 61',
          'Production': '63, 64, 65, 66, 67, 68, 69, 70',
          'Lighting': '72, 73, 74, 75',
          'Art department': '77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88',
          'Sound': '90, 91, 92, 93, 94, 95, 96'
        }
      }

      input_csv = CSV.read(input_file, headers: true)
      input_csv.each do |row|
        h = header.map { |el| [el, nil]}.to_h
        h['email'] = row['E-mail']
        h['job_type'] = row['Job type']
        h['first_name'] = row['First name']
        h['last_name'] = row['Last name']
        h['street'] = row['Street']
        h['number'] = row['Number']
        h['bus'] = row['Bus']
        h['city'] = row['City']
        h['postal_code'] = row['Postal code']
        h['phone_number'] = row['Phone number']
        h['birthday'] = row['Birthday']
        h['gender'] = row['Gender']
        h['nationality'] = row['Nationality']
        h['ethnicity'] = row['Ethnicity']
        h['languages'] = row['Languages']
        h['height'] = row['Height']
        h['weight'] = row['Weight']
        h['shoe_size'] = row['Shoe size']
        h['clothing_size'] = (row['Clothing size male'].blank? ? (row['Clothing size female'].blank? ? (row['Clothing size child'].blank? ? '' : row['Clothing size child']) : row['Clothing size female'].downcase) : row['Clothing size male'].downcase)
        h['eye_color'] = row['Eye color']
        h['hair_color'] = row['Hair color']
        h['hair_length'] = row['Hair length']
        h['hair_type'] = row['Hair type']
        h['haircut'] = row['Hair may be cut / colored']
        h['beard'] = row['Beard']
        h['mustache'] = row['Mustache']
        h['tattoos'] = row['Tattoos']
        h['piercings'] = row['Piercings']
        h['employment'] = row['Employment situation']
        h['driving_license'] = row['Driving license']
        h['own_transport'] = row['Own transport']
        h['previous_experience'] = row['Previous experience']
        h['available_for'] = row['Available for']
        h['talent_ids'] = (row['Type of talent'].to_s.split(', ') + row['Crew type'].to_s.split(', ')).join(', ')
        h['images'] = row['Photos']
        h['videos'] = row['Videos']

        attribute_maps.keys.each do |key|
          data = h[key.to_s]
          if data.present?
            attribute_maps[key].each do |k, v|
              # puts("#{k.class}: #{v.class}")
              data.sub!(k.to_s, v)
            end
            h[key.to_s] = data
          end
        end

        output_csv << h.values
      end
    end
  end
end
