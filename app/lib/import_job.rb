ImportJob = Struct.new(:row) do
  def perform
    email = row['email']

    user = User.find_by_email(email)

    if user.nil?
      user = User.new(email: email, import: true)
      # user.skip_confirmation!

      user_attributes = {
        profile_attributes: {
          id: user.profile.id,
          job_type: (row['job_type'] || '').split(/\W+/),
          first_name: row['first_name'],
          last_name: row['last_name'],
          phone_number: row['phone_number'],
          birthday: row['birthday'],
          gender: row['gender'],
          nationality: row['nationality'],
          ethnicity: (row['ethnicity'] || '').split(/\W+/),
          language: (row['languages'] || '').split(/\W+/),
          height: row['height'],
          weight: row['weight'],
          shoe_size: row['shoe_size'],
          clothing_size: ClothingSize[row['clothing_size']],
          eye_color: row['eye_color'],
          hair_color: row['hair_color'],
          hair_length: row['hair_length'],
          hair_type: row['hair_type'],
          haircut: row['haircut'],
          beard: row['beard'],
          mustache: row['mustache'],
          tattoos: row['tattoos'],
          piercings: row['piercings'],
          employment: row['employment'],
          driving_license: row['driving_license'],
          own_transport: row['own_transport'],
          previous_experience: row['previous_experience'],
          available_for: (row['available_for'] || '').split(/\W+/),
          address_attributes: {
            id: user.profile.address.id,
            street: row['street'],
            number: row['number'],
            bus: row['bus'],
            postal_code: row['postal_code'],
            city: row['city']
          }
        },
        talent_ids: (row['talent_ids'] || '').split(/\W+/)
      }

      user.assign_attributes user_attributes

      begin
        user.save validate: false
        UserMailer.imported(user).deliver_later
      rescue
        user.destroy
      end
    end

    old_videos = user.profile.videos.map { |video| video.url }
    old_images = user.profile.images.map { |image| image.file_identifier }

    videos = (row['videos'] || '').split(', ').reject { |video| old_videos.include? video }
    images = (row['images'] || '').split(', ').reject { |image| old_images.include? File.basename(URI.parse(image).path) }

    videos.each do |vid|
      user.profile.videos << Video.new(url: vid)
    end

    images.each do |img|
      user.profile.images << Image.new(remote_file_url: img)
    end
  end
end
