module SQLHelper
  def self.db_concat(*args)
    adapter = ActiveRecord::Base.configurations[Rails.env]['adapter'].to_sym
    args.map!{ |arg| arg.class==Symbol ? arg.to_s : "'#{arg}'" }

    case adapter
    when :mysql, :mysql2
        "CONCAT(#{args.join(',')})"
      when :sqlserver
        args.join('+')
      else
        args.join('||')
    end

  end
end
