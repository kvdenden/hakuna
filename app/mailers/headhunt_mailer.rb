class HeadhuntMailer < ApplicationMailer

  def headhunted(email)
    @email = email
    mail(to: @email, subject: 'Welcome to Hakuna Casting')
  end

end
