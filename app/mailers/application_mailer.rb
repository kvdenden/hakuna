class ApplicationMailer < ActionMailer::Base
  default from: 'Hakuna Casting <noreply@hakuna.be>'
  layout 'mailer'
end
