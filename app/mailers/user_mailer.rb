class UserMailer < ApplicationMailer

  # send email after user is successfully imported
  def imported(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome to Hakuna Casting')
  end

  def registered(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome to Hakuna Casting')
  end
end
