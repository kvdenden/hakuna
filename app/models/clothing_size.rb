class ClothingSize < ActiveEnum::Base
  value name: 'xs32_f', type: :female
  value name: 'xs34_f', type: :female
  value name: 's36_f', type: :female
  value name: 'm38_f', type: :female
  value name: 'l38_40_f', type: :female
  value name: 'l42_f', type: :female
  value name: 'l42_44_f', type: :female
  value name: 'l44_f', type: :female
  value name: 'xl46_f', type: :female

  value name: 'xs44_m', type: :male
  value name: 'xs46_m', type: :male
  value name: 's48_m', type: :male
  value name: 'm50_m', type: :male
  value name: 'l52_m', type: :male
  value name: 'xl54_m', type: :male

  value name: '80_ch', type: :child
  value name: '86_ch', type: :child
  value name: '92_ch', type: :child
  value name: '98_ch', type: :child
  value name: '104_ch', type: :child
  value name: '110_ch', type: :child
  value name: '116_ch', type: :child
  value name: '122_ch', type: :child
  value name: '128_ch', type: :child
  value name: '134_ch', type: :child
  value name: '140_ch', type: :child
  value name: '146_ch', type: :child
  value name: '152_ch', type: :child
  value name: '158_ch', type: :child
  value name: '164_ch', type: :child

  def self.to_select type=nil
    if type.nil?
      super()
    else
      all.select { |id, name, meta| meta[:type] == type.to_sym }.map { |id, name, meta| [name, id] }
    end
  end
end
