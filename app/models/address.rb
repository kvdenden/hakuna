class Address < ActiveRecord::Base
  belongs_to :addressable

  validates :street, presence: true
  validates :number, presence: true
  validates :city, presence: true
  validates :postal_code, presence: true
  validates :country, presence: true

  def to_s
    "#{street} #{bus.blank? ? number.to_s : number.to_s + '/' + bus.to_s},\n#{postal_code} #{city},\n#{country_name}"
  end

  def country_name
    c = ISO3166::Country[country]
    c.translations[I18n.locale.to_s] || c.name if c
  end
end
