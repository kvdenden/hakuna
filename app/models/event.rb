class Event < ActiveRecord::Base
  has_one :image
  has_and_belongs_to_many :users

  accepts_nested_attributes_for :image, reject_if: lambda { |i| i[:id].nil? && i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  validates :title, presence: true
  validates :date, presence: true

  scope :upcoming, -> { where(:date.gte => Date.today.begnning_of_day) }
end
