class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable

  has_one :profile, dependent: :destroy

  has_and_belongs_to_many :lists
  # has_and_belongs_to_many :events

  has_one :comment, dependent: :destroy

  accepts_nested_attributes_for :profile

  after_initialize :init_profile, :if => :new_record?

  has_and_belongs_to_many :talents

  validates :terms_and_conditions, acceptance: true

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where.not(active: true) }
  scope :admin, -> { active.where(admin: true) }
  scope :cast, -> { active.where.not(admin: true).joins(:profile).where("profiles.job_type LIKE ?", '%cast%') }
  scope :crew, -> { active.where.not(admin: true).joins(:profile).where("profiles.job_type LIKE ?", '%crew%') }

  scope :talent_eq, -> (talent=1) { joins(:talents).where(talents: {id: Talent.find(talent).subtree_ids}).distinct }

  scope :full_name, -> (name) { joins(:profile).where("#{SQLHelper.db_concat(:first_name, ' ', :last_name)} LIKE ?", "%#{name}%") }

  scope :profile_age_gteq, -> (age=1) { joins(:profile).where("profiles.birthday <= ?", age.to_i.years.ago )}
  scope :profile_age_lteq, -> (age=1) { joins(:profile).where("profiles.birthday >= ?", (age.to_i+1).years.ago + 1.day )}

  scope :city, -> (city) { joins(profile: :address).where("addresses.city LIKE ?", "%#{city}%")}

  def init_profile
    build_profile if profile.nil?
  end

  def full_name
    profile.nil? ? email : "#{profile.first_name} #{profile.last_name}"
  end

  def cast?
    profile.job_type.include? "cast"
  end

  def crew?
    profile.job_type.include? "crew"
  end

  def active_for_authentication?
    super and self.active?
  end

  def update_with_password(params={})
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end
    result = update_attributes(params)
    clean_up_passwords
    return result
  end

  private

  def self.ransackable_scopes(auth_object=nil)
    %i(talent_eq full_name profile_age_gteq profile_age_lteq city)
  end
end
