class Video < ActiveRecord::Base

  belongs_to :videoable
  acts_as_list scope: :videoable

  validates :url, presence: true

  def embed_code(classes='')
    begin
      "<div class='#{classes}'>#{VideoInfo.new(url).embed_code}</div>"
    rescue
      "<div><a href='#{url}' target='_blank'>#{url}</a></div>"
    end
  end
end
