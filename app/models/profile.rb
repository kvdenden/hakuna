class Profile < ActiveRecord::Base
  extend Enumerize

  belongs_to :user
  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address
  has_many :images, -> { order(position: :asc) }, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :images, reject_if: lambda { |i| i[:id].nil? && i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  has_many :videos, -> { order(position: :asc) }, as: :videoable, dependent: :destroy
  accepts_nested_attributes_for :videos, reject_if: lambda { |i| i[:url].blank? }, allow_destroy: true

  serialize :job_type, Array
  serialize :ethnicity, Array
  serialize :available_for, Array

  serialize :language, Array

  enumerize :job_type, in: [:cast, :crew], multiple: true
  enumerize :gender, in: [:male, :female]
  enumerize :ethnicity, in: [:african, :asian, :eastern_european, :indian, :latin_american, :mediterraenan, :middle_eastern, :native_american, :north_african, :scandinavian, :white], multiple: true
  enumerize :eye_color, in: [:light_blue, :bright_blue, :dark_brown, :light_brown, :green, :gray, :mixed]
  enumerize :hair_color, in: [:black, :very_dark_brown, :brown, :light_brown, :dark_blonde, :light_blonde, :platinum_blonde, :ginger, :red, :other]
  enumerize :hair_length, in: [:bald, :very_short, :short, :medium, :long, :very_long]
  enumerize :hair_type, in: [:straight, :curly, :wavy, :kinky]
  enumerize :employment, in: [:student, :employee, :self_employed, :retired, :unemployed]
  enumerize :available_for, in: [:swimwear, :underwear, :unrecognizable_nude, :artistic_nude, :bodypaint], multiple: true

  enumerate :clothing_size, with: ClothingSize


  validates :job_type, presence: true
  validate do
    errors.add(:job_type, 'can\'t be blank') if job_type.empty?
  end
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone_number, presence: true
  validates :phone_number, format: /\+[0-9]{8,}/
  validates :birthday, presence: true
  validates :birthday, date: { before: Proc.new { Date.today }, message: 'must be before today' }
  validates :gender, presence: true
  validates :nationality, presence: true
  validates :ethnicity, presence: true
  validate do
    errors.add(:ethnicity, 'can\'t be blank') if ethnicity.empty?
  end
  validates :language, presence: true
  validate do
    errors.add(:language, 'can\'t be blank') if language.empty?
  end

  with_options if: 'job_type.cast?' do |cast|
    cast.validates :height, presence: true
    cast.validates :height, numericality: { greater_than: 0, less_than_or_equal_to: 240 }
    cast.validates :weight, presence: true
    cast.validates :weight, numericality: { greater_than: 0 }
    cast.validates :shoe_size, presence: true
    cast.validates :shoe_size, numericality: { greater_than: 0, less_than_or_equal_to: 50 }
    cast.validates :clothing_size, presence: true
    cast.validates :eye_color, presence: true
    cast.validates :hair_color, presence: true
    cast.validates :hair_length, presence: true
    cast.validates :hair_type, presence: true
  end

  with_options if: 'job_type.crew?' do |crew|
  end

  validates :employment, presence: true

  validate do
    if images.select {|d| !d.marked_for_destruction? }.count < 2
      errors.add(:images, 'missing (add at least 2)')
    end
  end

  validates_associated :address

  after_initialize :init_address, :if => :new_record?

  def init_address
    build_address if address.nil?
  end

end
