class Image < ActiveRecord::Base
  mount_uploader :file, ImageUploader
  belongs_to :imageable, polymorphic: true
  acts_as_list scope: :imageable

  validates :file, presence: true
end
