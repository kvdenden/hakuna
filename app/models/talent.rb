class Talent < ActiveRecord::Base
  extend FriendlyId
  friendly_id :to_s, use: :slugged

  has_ancestry orphan_strategy: :adopt
  default_scope { order('position ASC') }

  has_and_belongs_to_many :users

  scope :cast, -> { find friendly.find('cast').subtree_ids }
  scope :crew, -> { find friendly.find('crew').subtree_ids }

  def to_s
    if title.downcase == "other" && parent.present?
      "#{title} (#{parent.title})"
    else
      title
    end
  end
end
