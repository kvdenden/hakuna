module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
        concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
          concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
          concat content_tag(:span, 'Close', class: 'sr-only')
        end)
        concat message
      end)
    end
    nil
  end

  def icon type, klass=''
    "<span class='glyphicon glyphicon-#{type} #{klass if klass}' aria-hidden='true'></span>".html_safe
  end

  def country_name country_code
    country = ISO3166::Country[country_code]
    country.translations[I18n.locale.to_s] || country.name
  end

  def nested_talent_options user, nodes = Talent.roots.to_a, parent = nil, depth = 0, collapsed = true
    result = ""
    nodes.each_with_index do |node, index|
      if node.has_children?

        collapse_tree = collapsed && (index > 0 || depth > 0)
        # tree
        result += content_tag :li, class: "list-item list-position-#{index+1} tree" do
          content = content_tag(:span, ' ', class: 'indent') * depth
          content += link_to "#{content_tag :span, '', class: 'chevron'} #{node.title}".html_safe, "#collapse-#{node.id}", {class: "collapse-toggler #{'collapsed' if collapse_tree}", 'data-toggle': 'collapse', 'aria-expanded': false, 'aria-controls': "collapse-#{node.id}"}
          content.html_safe
        end
        result += content_tag :div, id: "collapse-#{node.id}", class: "list-item collapse subtree #{'in' unless collapse_tree}" do
          nested_talent_options user, node.children, node, depth + 1, collapsed
        end
      else
        # leaf
        # "<div class='checkbox'><label for='' class='choice'</div>"
        result += content_tag :li, class: "list-item list-position-#{index+1} leaf" do
          content_tag :label, class: 'choice' do
            content = content_tag(:span, ' ', class: 'indent') * depth
            content += check_box_tag("user[talent_ids][]", node.id, user.talents.include?(node), id: "user[talent_ids][#{node.id}]")
            content += " #{node.title}"
            content.html_safe
          end
        end
      end
    end
    if depth == 0
      return content_tag :ul, class: 'list-unstyled' do
        content = ""
        content += hidden_field_tag("user[talent_ids][]", nil)
        content += result
        content.html_safe
      end
    else
      return result.html_safe
    end
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
