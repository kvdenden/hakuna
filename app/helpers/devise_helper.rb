module DeviseHelper
  def devise_error_messages!
  return '' if resource.errors.empty?

  messages = resource.errors.messages.map { |key, msgs| msgs.map{ |msg| content_tag(:li, "#{key.to_s.split('.').last.humanize} #{msg}") } }.flatten.join
  sentence = I18n.t('errors.messages.not_saved',
  count: resource.errors.count,
  resource: resource.class.model_name.human.downcase)

  # for Bootstrap 3 pay attention to the classes in the "div"

  html = <<-HTML
  <div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4>#{sentence}</h4>
  #{messages}</div>
  HTML
  html.html_safe
 end
end
